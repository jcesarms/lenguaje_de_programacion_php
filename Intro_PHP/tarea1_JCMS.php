<html>
    <head>
        <title>PIRAMIDE</title>
        <link href=style.css rel="stylesheet">
    </head>
    <body >
        <?php

        
            $fila = 30;
            $cadena = "";
            $contador = 0;
            //PIRAMIDE
            echo "<center> PIRAMIDE </center>";
            for ($i=0; $i < $fila; $i++) { 
                    while($contador <= $i){
                        $cadena .= "*";
                        $contador++;
                    }
                    echo "<center id=cadena>".$cadena."</center>";
                    $cadena ="";
                    $contador =0;
            }
            //ROMBO
            $bandera =false;
            echo "<center> ROMBO </center>";
            for ($i=0; $i <= $fila; $i++) { 
                if($bandera != true ){
                    while($contador <= $i){
                        $cadena .= "*";
                        $contador++;
                        if($i === $fila/2){
                            $bandera = true;
                            break;
                        }
                    }
                }
                if($bandera == true ){
                    $contador = $fila;
                    while($contador > $i){
                        $cadena .= "*";
                        $contador--;
                    }
                }
                echo "<center id=cadena>".$cadena."</center>";
                $cadena ="";
                $contador =0;
            }

        ?>
    </body>
</html>