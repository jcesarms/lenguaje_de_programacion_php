<?php
    session_start();
?>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="info.php">Home </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="formulario.php">Registrar Alumno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cerrar.php">Cerrar Sesión</a>
                </li>
            </ul>
        </div>
    </nav>
    <main>
        <div class="container mt-3">
            <form accion="info.php" method="POST">
                <div class="form-group row">
                    <label for="cuenta" class="col-sm-2 col-form-label">Número de cuenta</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="cuenta" placeholder="Número de cuenta">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="primer" class="col-sm-2 col-form-label">Primer Apellido</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="primer" placeholder="Primer Apellido">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="segundo" class="col-sm-2 col-form-label">Segundo Apellido</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="segundo" placeholder="Segundo APellido">
                    </div>
                </div>
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Género</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gridRadios" id="hombre" value="option1">
                                <label class="form-check-label" for="hombre">
                                    Hombre
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gridRadios" id="mujer" value="option2">
                                <label class="form-check-label" for="mujer">
                                    Mujer
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gridRadios" id="otro" value="option3">
                                <label class="form-check-label" for="otro">
                                    Otro
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label for="fecha" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="fecha">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="contraseña" class="col-sm-2 col-form-label">Contraseña</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="contraseña" placeholder="Contraseña">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </div>
            </form>
        </div>
    </main>
</body>

</html>