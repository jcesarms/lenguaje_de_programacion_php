<?php 
    session_start();
    $_SESSION['Alumno'] = [
        1 => [
             'num_cta' => '1',
             'nombre' => 'Admin',
             'primer_apellido' => 'General',
             'segundo_apellido' => '',
             'cantrasenia' => 'adminpass123',
             'genero' => 'O',
             'fecha_nac' => '25/01/1990',
        ]
    ];
?>

<html>
    <head>
        <title>LOGIN</title>
        <link rel="stylesheet" href="css/login.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class = "contenedor">
            <div class= "contenedor-login">
                <h2 class = "titulo">Iniciar sesión</h2>
                    <form action="info.php" method="POST">
                        <div class="form-group">
                            <label for="input-text">Numero de cuenta:</label>
                            <input name="texto" class="input" type="text" id="input-cuenta" placeholder="Cuenta">
                        </div>
                        <div class="form-group">
                            <label for="input-password">Contraseña:</label0>   
                            <input name="password" class="input" type="password" id="input-password" placeholder="Contraseña">
                        </div>
                       
                        <input type='submit' class="btn" value="Enviar"/>
                         
                    </form>
            </div>
        </div>
    </body>
</html>