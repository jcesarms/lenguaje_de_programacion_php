//Realizar una expresión regular que detecte emails correctos.
/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
/^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)$/i


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
^[a-zA-Z0-9]{60}$

//Crea una funcion para escapar los simbolos especiales.

<?php
function escape($entrada) { 
    if(is_array($entrada)) 
        return array_map(__METHOD__, $entrada); 

    if(!empty($entrada) && is_string($entrada)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $entrada); 
    } 
    return $entrada; 
} 
?>

//Crear una expresion regular para detectar números decimales.
^[0-9]+(\.[0-9]+)?$